---
title: Sarien Chromebooks
---

`sarien` is a board name for x86_64-based Chromebooks. Some examples are:

  - [Dell Latitude 5400 Chromebook Enterprise](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/dell-latitude-5400-chromebook-enterprise/spd/latitude-14-5400-chrome-laptop/xctolc540014us1)
  - [Dell Latitude 5300 2-in-1 Chromebook Enterprise](https://www.dell.com/en-us/work/shop/cty/pdp/spd/latitude-13-5300-2-in-1-chrome-laptop)

The specs for these chromebooks vary in terms of format, display, connectivity
and devices, but they are based on Intel Whiskey Lake 64 bit CPUs
such as the Celeron 4305U.

### Debugging interfaces

The Dell Latitude 5400 Chromebook Enterprise has been flashed and tested
with a [Servo Micro](../../01-debugging_interfaces) interface and a
RTL8152-based USB-Ethernet adapter, as well as with a combination of
Servo Micro and Servo v4 as documented in [this
page](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_micro.md). Note
that `sarien` Chromebooks don't support [CCD](../../02-ccd).

#### Network connectivity

An external USB-Ethernet dongle based on the Realtek RTL8152 can be used
to provide Ethernet connectivity in Depthcharge. Alternatively, A Servo v4 can be
used together with the Servo Micro to provide an Ethernet interface (see
the [official
docs](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_micro.md)).

Note that to be able to use the RTL8152 USB-Eth adapter, the kernel must
be configured with `CONFIG_USB_RTL8152=y`.

#### Known issues

After issuing a `cold_reset:on` command with `dut-control`, this device
can't be turned on again with `cold_reset:off`. The recommended way to
turn it off and on is with the `power_state:off` and `power_state:on`
commands.

See [Common issues](../common_issues.md) as well.

### Example kernel command line arguments

```
earlyprintk=uart8250,mmio32,0xde000000,115200n8 console_msg_format=syslog console=ttyS0,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

The IP and path of the NFS share are examples and should be adapted to
the test setup.
