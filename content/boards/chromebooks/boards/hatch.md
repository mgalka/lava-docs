---
title: Hatch Chromebooks
---

`hatch` is a board name for x86-64 Chromebooks using an Intel Core i5-10210U.

The `hatch` Chromebook we're currently using in the lab is an [Asus Flip C436FA](https://www.asus.com/2-in-1-PCs/ASUS-Chromebook-Flip-C436FA/)
(codename `helios`).

### Debugging interfaces

`hatch` boards have been flashed and tested with [Servo
v4](../../01-debugging_interfaces) interfaces.

The Asus Chromebook Flip C436FA supports CCD and can be debugged through
its USB-C port on the left side (power supply's one).

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

No specific issues found for this Chromebook yet, but see [Common
issues](../common_issues.md).

### Example kernel command line arguments

An example of kernel command line arguments to boot a FIT image:

```
earlyprintk=uart8250,mmio32,0xde000000,115200n8 console=ttyS0,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

The IP and path of the NFS share are examples and should be adapted to
the test setup.
