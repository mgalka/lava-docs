---
title: Chromebooks
---

[Chromebooks](https://www.google.com/intl/en_uk/chromebook/) are laptops
or tablets that run Chrome OS, a Linux-based operating system by Google
and use the Google Chrome browser as a user interface.

Although they are very diverse in terms of hardware design, their
low-level debug interfaces are very similar using [Servo
boards](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/refs/heads/master/docs/servo.md)
to access low-level controls such as hardware reset, serial consoles and
to flash the firmware.
