---
title: Running Chrome OS in LAVA
weight: 3
---

Running Chrome OS images and dedicated Chrome OS tests come with very specific
requirements.  These tests typically can't be run with any other user-space
than Chrome OS and have to be deployed via the [Tast
framework](https://chromium.googlesource.com/chromiumos/platform/tast/).

## Flashing a Chrome OS image on a Chromebook

The first step is to flash a Chrome OS image onto a Chromebook, as they can't
be booted over NFS.  This can be done within a Debian Buster NFS root with no
special tools installed.  It can be entirely automated in a LAVA job.  Once the
device has booted, the Chrome OS image needs to be downloaded (public location
yet to be determined).  Then here's a sample script to flash the image from
within an NFS root job, assuming the image is called
`octopus-chromiumos-test-image.bin`:

```sh
cd /root
mkdir chromeos
dev=$(losetup -P -f --show octopus-chromiumos-test-image.bin)
mount "$dev"p3 chromeos -o ro
cd chromeos
mount udev -t devtmpfs dev
mount proc -t proc proc
mount sysfs -t sysfs sys
mount -t tmpfs tmpfs tmp
mount --bind /root/ mnt/empty
mount
ls -l mnt/empty/octopus-chromiumos-test-image.bin
ls -l dev/mmc*
echo "Starting to flash..."
chroot . \
  /usr/sbin/chromeos-install \
  --dst /dev/mmcblk0 \
  --payload_image /mnt/empty/octopus-chromiumos-test-image.bin \
  --yes
```

It relies on the `chromeos-install` script which is provided inside the image
itself.  This is done by accessing the main partition (read-only) in the image
via losetup and chroot.  The image file is made available "within itself" by
bind-mounting it in a known empty directory `/mnt/empty`.

## Rootfs for flashing Chrome OS using Debos

Rootfs image recipe can be obtained by following below steps

```
$ git clone https://gitlab.collabora.com/lava/collabora-lava-setup
$ sudo docker run -it -v $(pwd):/chromeos --device /dev/kvm --privileged -w /chromeos kernelci/debos bash
# debos tools/chromeos/chromeos-rootfs.yml
```

and resulting image can be found at `/chromeos/debian-buster-arm64.tgz`.

## Flashing a Chrome OS image using LAVA:

Once rootfs is ready, then we need to add Chrome OS image file added by hand since its not available via HTTP
server yet. Finally we can run LAVA job to flash Chrome OS image on octopus device. The flashing script `chromeos-flash.sh`
looks for the image in `/root` directory of the rootfs. By default, the `chromeos-flash.sh` script, uses `chromiumos_test_image.bin` but
you can also pass specific image as a CLI argument e.g. `chromeos-flash.sh my_image_file.bin`.

LAVA job definition for flashing ChromeOS can be found below and its taken from [here](https://staging.lava.collabora.dev/scheduler/job/15006/definition)

```
device_type: hp-x360-12b-ca0500na-n4000-octopus

context:
  extra_kernel_args: console_msg_format=syslog cros_secure

job_name: Chrome OS boot 5.10 - octopus
timeouts:
  job:
    minutes: 60
  action:
   minutes: 60
  actions:
    power-off:
      seconds: 30
priority: high
visibility: public


actions:
- deploy:
    timeout:
      minutes: 15
    to: tftp
    kernel:
      url: https://storage.kernelci.org/stable/linux-5.10.y/v5.10.50/x86_64/x86_64_defconfig+x86-chromebook/gcc-8/kernel/bzImage
    modules:
      url: https://storage.kernelci.org/stable/linux-5.10.y/v5.10.50/x86_64/x86_64_defconfig+x86-chromebook/gcc-8/modules.tar.xz
      compression: xz
    nfsrootfs:
      url: file:///tmp/debian-buster-amd64.tar
    ramdisk:
      url: http://storage.kernelci.org/images/rootfs/debian/buster/20210503.0/amd64/initrd.cpio.gz
      compression: gz
    os: oe


- boot:
    timeout:
      minutes: 15
    method: depthcharge
    commands: nfs
    prompts:
      - '/ #'


- test:
    timeout:
      minutes: 60
    definitions:
    - repository:
        metadata:
          format: Lava-Test Test Definition 1.0
          name: hack
          description: "hack"
          os:
            - debian
          scope:
            - functional
          environment:
            - lava-test-shell
        run:
          steps:
            - lsblk
            - ls -l /root
            - /bin/bash /usr/bin/chromeos-flash.sh
      lava-signal: kmsg
      from: inline
      name: hack
      path: inline/hack.yaml
```


## Booting with Chrome OS

Some particular kernel configurations need to be enabled in order for all the
Chrome OS services to fully boot.

_To be continued_

## Running tests with Tast

Once the device has booted Chrome OS, it's possible to start using Tast over
SSH from a separate host (e.g. a LAVA dispatcher).  Tast can be run within
Docker on the host, and this is the approach take with LAVA jobs too.

_To be continued_
